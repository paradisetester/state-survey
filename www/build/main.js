webpackJsonp([0],{

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home_page__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupPage = (function () {
    function SignupPage(fb, navCtrl, auth) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.form = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].email])],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].minLength(6)])]
        });
    }
    SignupPage.prototype.signup = function () {
        var _this = this;
        var data = this.form.value;
        var credentials = {
            email: data.email,
            password: data.password
        };
        this.auth.signUp(credentials).then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home_page__["a" /* HomePage */]); }, function (error) { return _this.signupError = error.message; });
    };
    SignupPage.prototype.loginbtn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'as-page-signup',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\signup\signup.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Signup</ion-title>\n		</ion-navbar>\n</ion-header>\n\n<ion-content class="page-signup">\n	\n<section class="form-section">\n	<div class="form-inner">\n		<div class="form-graphic">\n			<img src="../../assets/images/vector.png">\n		</div>\n		<div class="form-design">\n			<img src="../../assets/images/logo.png">\n			\n			<form (ngSubmit)="signup()" [formGroup]="form" autocomplete="off">\n				<p class="form_heading">Signup</p>\n				<ion-list inset>\n					\n					<ion-item [ngClass]="{ invalid: emailErrors.hasError(\'*\', [\'touched\']) }">\n						<ion-input type="text" placeholder="Email" formControlName="email" autocomplete="on" ></ion-input>\n					</ion-item>\n\n					<div ngxErrors="email" #emailErrors="ngxErrors">\n						<div [ngxError]="[\'email\', \'required\']" [when]="[\'touched\']">It should be a valid email</div>\n					</div>\n\n					<ion-item [ngClass]="{ invalid: passwordErrors.hasError(\'*\', [\'touched\']) }">\n						<ion-input type="password" placeholder="Password" formControlName="password"></ion-input>\n					</ion-item>\n\n					<div ngxErrors="password" #passwordErrors="ngxErrors">\n						<div [ngxError]="[\'minlength\', \'required\']" [when]="[\'touched\']">It should be at least 6 characters</div>\n					</div>\n				</ion-list>\n\n				<div padding-horizontal>\n					<div class="form-error">{{signupError}}</div>\n\n					<button ion-button full type="submit" [disabled]="!form.valid">Sign up</button>\n				\n				\n				<ion-list class="margin-top">\n\n					<button ion-button icon-left block clear (click)="loginbtn()">\n						<ion-icon name="person-add"></ion-icon>\n						Login\n					</button>\n				</ion-list>\n				</div>\n			</form>\n			\n		</div>\n	</div>\n</section>\n\n</ion-content>'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\signup\signup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TelehealthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TelehealthPage = (function () {
    function TelehealthPage(nav, navCtrl, restProvider, http, auth) {
        this.nav = nav;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.auth = auth;
        this.getStates();
        this.getCategory();
    }
    TelehealthPage.prototype.ionViewDidLoad = function () {
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories1', 'telehealth', '2');
        }, TIME_IN_MS);
    };
    TelehealthPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getStateTelehealth()
            .then(function (data) {
            _this.states = data;
        });
    };
    ;
    TelehealthPage.prototype.getCategory = function () {
        var _this = this;
        this.restProvider.getCategoryTelehealth()
            .then(function (data) {
            _this.category = data;
        });
    };
    ;
    TelehealthPage.prototype.onSelectChange = function (selectedValue) {
        stateChange(selectedValue);
    };
    TelehealthPage.prototype.onCategoryChange = function (selectedValue) {
        categoryChange(selectedValue);
    };
    TelehealthPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    TelehealthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-telehealth',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\telehealth\telehealth.html"*/'<ion-content> \n<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<div class="logo">\n			<img src="../../assets/images/logo.jpg">\n		</div>\n	</ion-navbar>\n</ion-header>\n	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">\n	\n<ion-content> \n	<!---header--->	\n	<header>\n		<div class="header_section">\n			<div class="container">\n				<div class="logo">\n					<img src="../../assets/images/logo.jpg">\n				</div>\n				\n				<div class="nav">\n					<ul>\n						<li><a href="/#/340b">340B</a></li>\n						<li><a href="/#/telehealth" class="active">Telehealth</a></li>\n						<li><a href="/#/1115states">1115 States</a></li>\n						<li><a href="/#/home">Sud</a></li>\n						<li><a (click)="logout()" *ngIf="auth.authenticated">\n							Log out</a>\n						</li>\n							\n						<li><a (click)="login()" *ngIf="!auth.authenticated">\n							Log in</a>\n						</li>\n					</ul>\n				</div>\n				<h4>substance  use disorder</h4>\n			</div>\n		</div>\n	</header>\n	\n	\n	<!------>\n	\n	<!---section-->\n	<section>\n		\n		<div class="medi_coverage">\n			<img src="../../assets/images/energy.png">\n		</div>\n		<div class="sec_outr">\n			<div class="container">\n				<h5> State Telehealth Laws and Medicaid Policies: <br> 50-State Survey Findings</h5>\n				<h6>11.11.18</h6>\n				<a class="butttonn" href="">Download Attachment</a>\n				<div class="desprective-content">\n						 <ul class="research-img">\n								<li>\n								<a href="#">\n								 <img src="../../assets/images/Jacqueline.jpg">\n								</a>\n								 <div class="research_info">\n									 <h6>Jacqueline D. Marks</h6>  \n									 <p>MANAGER</p>\n								 </div>\n								</li>\n								<li>\n								<a href="#">\n								 <img src="../../assets/images/Augenstein_Jared.jpg">\n								</a>\n								 <div class="research_info">\n									 <h6>Jared Augenstein</h6>  \n									 <p> SR MANAGER</p>\n								 </div>\n								</li>\n								<li>\n								<a href="#">\n								 <img src="../../assets/images/Seigel_Randi.jpg">\n								</a>\n								 <div class="research_info">\n									 <h6>Randi Seigel</h6>  \n									 <p>Partner</p>\n								 </div>\n								</li>\n						 </ul>\n							<p>The use of telemedicine services is growing nationwide as providers and payors seek to improve access and better manage patient care, while reducing health care spending. State laws and Medicaid<sup><a style="color:undefined" href="javascript:void(0)" class="state_check" data-tab="sourcepara">[1]</a></sup> policies related to reimbursement, licensure and practice standards are rapidly evolving in response to the proliferation of technology and the growing evidence base that demonstrates the impact of telemedicine on access, quality and cost of care. Some states have been proactive in encouraging the use of telemedicine as a means to enhance services in rural areas, increase access to care for members with complex conditions, and reduce costs associated with unnecessary emergency department visits.</p>\n					 <p>In light of this rapidly changing landscape, Manatt Health has conducted a 50-state survey of state laws and Medicaid program policies related to telemedicine in the following key areas:</p>\n					 <ul class="prod_list">\n					 <li>Practice standards and licensure</li>\n					 <li>Coverage and reimbursement</li>\n					 <li>Eligible patient settings</li>\n					 <li>Eligible provider types</li>\n					 <li>Eligible technologies, and</li>\n					 <li>Service limitations.</li>\n					 </ul>\n					 <p>Based on survey results, we classified state telemedicine policies as \n						"progressive," "moderate," or "restrictive" across each of these categories, as captured in the state map below.</p>\n					 <p>Our analysis suggests that telemedicine will be critical to delivering health care in the future, and state Medicaid policies are evolving - in some states more quickly than others - to accelerate adoption of telemedicine models. As technology advances and the evidence base for telemedicine expands, state policy will continue to evolve to integrate telemedicine into payment and delivery reforms that support overarching program objectives related to access, quality, and cost of care.</p>\n					 <p>Additional findings from our analysis can be found here.</p>\n					 <p>To access a detailed summary profile of a state\'s telehealth laws and Medicaid policies, click the state on the map below. To explore state classifications across survey categories, use the State Classification Category drop down menu or hover your mouse over a state of interest. Explore further by using the controls at the top left of the map that allow you to zoom in/out, pan or type a state name in the search bar to quickly access a state of interest.</p>\n				 </div>\n			</div>\n			\n			<div class="dropdownn_Sec">\n				<div class="container">\n					<h5>Select States</h5>\n					<div class="dropdown_Sec">\n						<div class="container">\n							<ion-item>\n								<ion-label>States</ion-label>\n								 <ion-select name="statename" (ionChange)="onSelectChange($event)" >\n									<ion-option *ngFor="let state of states" value="{{state.abbreviation}}" >{{state.name}}</ion-option>\n								  </ion-select>\n							</ion-item>\n							<ion-item>\n								<ion-label>Categories</ion-label>\n								 <ion-select name="catname" (ionChange)="onCategoryChange($event)" >\n									<ion-option *ngFor="let cat of category" value="{{cat.title}}" >{{cat.title}}</ion-option>\n								  </ion-select>\n							</ion-item>\n						</div>\n					</div>\n					<div class="drop_Sec">\n						<i class="fas fa-question-circle"></i>\n						<a class="show_map" onclick="show_map(2)"> <i class="fas fa-globe-americas" ></i></a>\n					</div>\n				</div>\n			</div>\n		</div>\n		\n		<div class="map_section">\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map2"></div>       \n		</div>\n		\n		<div id="state_data"></div>\n		\n	     \n	</section>\n	<!--section-->\n	<!----footer-->\n    <footer>\n		<div class="foot_section">\n			<img src="../../assets/images/logo.jpg">\n			<ul class="foot-nav">\n				<li><a>Privacy</a></li>\n				<li><a href="/#/340b">340B</a></li>\n				<li><a href="/#/1115states" >1115 States</a></li>\n			</ul>\n			<ul>\n				<li><a><i class="fab fa-facebook-f"></i></a></li>\n				<li><a><i class="fab fa-twitter"></i></a></li>\n				<li><a><i class="fab fa-linkedin-in"></i></a></li>\n			</ul>\n		</div>\n	</footer>\n	<!----footer-->\n	\n</ion-content>	'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\telehealth\telehealth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]])
    ], TelehealthPage);
    return TelehealthPage;
}());

//# sourceMappingURL=telehealth.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 333:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 333;

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return firebaseConfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Config = (function () {
    function Config() {
        this.ApiUrl = '';
    }
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], Config);
    return Config;
}());

;
var firebaseConfig = {
    fire: {
        apiKey: "AIzaSyABBPPTvbVSmy9tZ7U6hkz8HZBEaJJNeqI",
        authDomain: "state-a3476.firebaseapp.com",
        databaseURL: "https://state-a3476.firebaseio.com",
        projectId: "state-a3476",
        storageBucket: "state-a3476.appspot.com",
        messagingSenderId: "811418715572"
    }
};
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var StatesPage = (function () {
    function StatesPage(nav, navCtrl, restProvider, http, auth) {
        this.nav = nav;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.auth = auth;
        this.getStates();
    }
    StatesPage.prototype.ionViewDidLoad = function () {
        console.log('ad');
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories3', '1115', '3');
        }, TIME_IN_MS);
    };
    StatesPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState1115()
            .then(function (data) {
            _this.states = data;
        });
    };
    ;
    StatesPage.prototype.onSelectChange = function (selectedValue) {
        checkJson1115(selectedValue);
    };
    StatesPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    StatesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-states',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\states\states.html"*/'<ion-content> \n<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<div class="logo">\n			<img src="../../assets/images/logo.jpg">\n		</div>\n	</ion-navbar>\n</ion-header>\n	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">\n	\n<ion-content> \n	<!---header--->	\n	<header>\n		<div class="header_section">\n			<div class="container">\n				<div class="logo">\n					<img src="../../assets/images/logo.jpg">\n				</div>\n				\n				<div class="nav">\n					<ul>\n						<li><a href="/#/340b">340B</a></li>\n						<li><a href="/#/telehealth">Telehealth</a></li>\n						<li><a href="/#/1115states" class="active">1115 States</a></li>\n						<li><a href="/#/home">Sud</a></li>\n						<li><a (click)="logout()" *ngIf="auth.authenticated">\n							Log out</a>\n						</li>\n							\n						<li><a (click)="login()" *ngIf="!auth.authenticated">\n							Log in</a>\n						</li>\n					</ul>\n				</div>\n				<h4>substance  use disorder</h4>\n			</div>\n		</div>\n	</header>\n	\n	\n	<!------>\n	\n	<!---section-->\n	<section>\n		\n		<div class="medi_coverage">\n			<img src="../../assets/images/energy.png">\n		</div>\n		<div class="sec_outr">\n			<div class="container">\n				<h5> Medicaid Coverage Waivers: State Profiles</h5>\n				<h6>13.11.18</h6>\n				<a class="butttonn" href="">Download Attachment</a>\n				<div class="desprective-content">\n						 \n							<p>Section 1115 demonstrations permit states to waive certain Medicaid statutory requirements to advance state policy priorities and test innovations in their Medicaid programs, provided the Secretary of Health and Human Services determines that the demonstration "furthers the goals of the Medicaid program" and is budget neutral. States have used 1115 waiver authority to implement demonstrations ranging from Medicaid managed care programs to delivery system and payment reform initiatives. In recent years, states have leveraged 1115 waivers to modify features of Medicaid coverage for the expansion population under the Affordable Care Act (ACA) as well as traditional Medicaid populations.\n\nThe Trump Administration is encouraging states to pursue flexibility in administering their Medicaid programs through 1115 demonstrations that modify eligibility requirements and benefit design. In January 2018, the Centers for Medicare and Medicaid Services (CMS) released long-anticipated guidance on crafting and implementing a work and community engagement requirement as a condition of Medicaid eligibility. Following the release of the guidance, CMS approved 1115 waivers from Arkansas, Indiana, Kentucky, and New Hampshire, the first four waivers to permit such requirements in the Medicaid program, and on June 1, 2018, Arkansas became the first state to launch its work and community engagement requirement. The future of work and community engagement requirements is in question after a U.S. District Court judge issued a ruling in Stewart v. Azar in late June that invalidated HHS\'s approval of Kentucky\'s Medicaid waiver for failing to consider how the waiver furthered the goals of the Medicaid program. In August, Medicaid beneficiaries in Arkansas filed a lawsuit challenging HHS\'s approval of that State\'s Medicaid waiver to implement work and community engagement requirements.\n\nThe Trump Administration has also recently begun to establish some guardrails on the changes to Medicaid that it will permit through 1115 waivers. In recent months, CMS has denied Kansas\'s request to impose lifetime Medicaid enrollment limits and Massachusetts\'s request to waive Section 1927 to implement a closed prescription drug formulary, while continuing to receive the rebates required by federal law. Additionally, CMS has stated it will not approve Arkansas\'s or Massachusetts\'s request to implement partial expansion-in which it would reduce its Medicaid eligibility level to 100 percent of the federal poverty level, while continuing to receive the enhanced federal medical assistance percentage authorized under the ACA-"at this time." The Administration\'s priorities will continue to emerge as CMS issues decisions on recently submitted state waiver proposals, including requests for work and community engagement requirements in non-expansion states, drug testing as a condition of Medicaid eligibility, and asset tests, among others.\n\nThis document inventories and compares the features of state Medicaid coverage waivers.</p>\n				 </div>\n			</div>\n			\n			<div class="dropdownn_Sec">\n				<div class="container">\n					<h5>Select States</h5>\n					<div class="dropdown_Sec">\n						<div class="container">\n							<ion-item>\n								<ion-label>States</ion-label>\n								 <ion-select name="statename" (ionChange)="onSelectChange($event)" >\n									<ion-option *ngFor="let state of states" value="{{state.name}}"  [disabled]="state.available == 0"  >{{state.name}}</ion-option>\n								  </ion-select>\n							</ion-item>\n							\n						</div> \n					</div>\n					<div class="drop_Sec">\n						<i class="fas fa-question-circle"></i>\n						<a  onclick="show_chart(3)"><i class="fas fa-chart-line " ></i></a>\n					<a onclick="show_map(3)"><i class="fas fa-globe-americas" ></i></a>\n					</div>\n				</div>\n			</div>\n		</div>\n		    \n		<div class="map_section">\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map3"></div>       \n		</div>\n		\n		<div id="state_data"></div>\n		\n		<div class="table_hidee">\n			<div class="table_hide">\n				<div id="example-table-theme-semantic-ui"></div>\n				<div class="guide">\n					<ul>\n						<li><span><img src="../../blue.png"></span><h5>= Waiver obtained </h5></li>\n						<li><span><img src="../../green.png"></span><h5> = Waiver requested</h5></li>\n						<li><span><img src="../../red.png"></span><h5>= No waiver required</h5></li>\n					</ul>\n				</div>\n				<div class="table-controls">\n					\n					<button id="download-xlsx">Download XLSX</button>\n					\n				</div>\n			</div>\n		</div>\n	 \n	</section>\n	<!--section-->\n	<!----footer-->\n    <footer>\n		<div class="foot_section">\n			<img src="../../assets/images/logo.jpg">\n			<ul class="foot-nav">\n				<li><a>Privacy</a></li>\n				<li><a href="/#/340b">340B</a></li>\n				<li><a href="/#/1115states" >1115 States</a></li>\n			</ul>\n			<ul>\n				<li><a><i class="fab fa-facebook-f"></i></a></li>\n				<li><a><i class="fab fa-twitter"></i></a></li>\n				<li><a><i class="fab fa-linkedin-in"></i></a></li>\n			</ul>\n		</div>\n	</footer>\n	<!----footer-->\n	\n</ion-content>	'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\states\states.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]])
    ], StatesPage);
    return StatesPage;
}());

//# sourceMappingURL=states.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FourtybPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FourtybPage = (function () {
    function FourtybPage(nav, navCtrl, restProvider, http, auth) {
        this.nav = nav;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.auth = auth;
        this.getStates();
    }
    FourtybPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState340b()
            .then(function (data) {
            _this.states = data;
        });
    };
    ;
    FourtybPage.prototype.onSelectChange = function (selectedValue) {
        checkJson340b(selectedValue);
    };
    FourtybPage.prototype.ionViewDidLoad = function () {
        USAmapLoad('usaTerritories4', '340b', '4');
    };
    FourtybPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    FourtybPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fourtyb',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\fourtyb\fourtyb.html"*/'<ion-content> \n<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<div class="logo">\n			<img src="../../assets/images/logo.jpg">\n		</div>\n	</ion-navbar>\n</ion-header>\n	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">\n	\n<ion-content> \n	<!---header--->	\n	<header>\n		<div class="header_section">\n			<div class="container">\n				<div class="logo">\n					<img src="../../assets/images/logo.jpg">\n				</div>\n				\n				<div class="nav">\n					<ul>\n						<li><a href="/#/340b" class="active">340B</a></li>\n						<li><a href="/#/telehealth">Telehealth</a></li>\n						<li><a href="/#/1115states">1115 States</a></li>\n						<li><a href="/#/home">Sud</a></li>\n						<li><a (click)="logout()" *ngIf="auth.authenticated">\n							Log out</a>\n						</li>\n							\n						<li><a (click)="login()" *ngIf="!auth.authenticated">\n							Log in</a>\n						</li>\n					</ul>\n				</div>\n				<h4>substance  use disorder</h4>\n			</div>\n		</div>\n	</header>\n	\n	\n	<!------>\n	\n	<!---section-->\n	<section>\n		\n		<div class="medi_coverage">\n			<img src="../../assets/images/energy.png">\n		</div>\n		<div class="sec_outr">\n			<div class="container">\n				<h5> 340B 50 State Survey</h5>\n				<h6>13.11.18</h6>\n				<a class="butttonn" href="">Download Attachment</a>\n				<div class="desprective-content">\n						 \n							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries\n\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>\n				 </div>\n			</div>\n			\n			<div class="dropdownn_Sec">\n				<div class="container">\n					<h5>Select States</h5>\n					<div class="dropdown_Sec">\n						<div class="container">\n							<ion-item>\n								<ion-label>States</ion-label>\n								 <ion-select name="statename" (ionChange)="onSelectChange($event)" >\n									<ion-option *ngFor="let state of states" value="{{state.name}}"   [disabled]="state.available == 0" >{{state.name}}</ion-option>\n								  </ion-select>\n							</ion-item>\n							\n						</div>\n					</div>\n					<div class="drop_Sec">\n						<i class="fas fa-question-circle"></i>\n						<a class="show_map" onclick="show_map(4)"><i class="fas fa-globe-americas" ></i></a>\n					</div>\n				</div>\n			</div>\n		</div>\n		 <div class="container">\n		<div class="map_section">\n			<div class="jsmaps-wrapper-box"><div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map4"></div>       </div>\n		</div>\n		\n		\n		<div class="material_outr">\n					<div id="state_data"></div>\n				</div>\n		 </div>\n	</section>\n	<!--section-->\n	<!----footer-->\n    <footer>\n		<div class="foot_section">\n			<img src="../../assets/images/logo.jpg">\n			<ul class="foot-nav">\n				<li><a>Privacy</a></li>\n				<li><a href="/#/340b">340B</a></li>\n				<li><a href="/#/1115states" >1115 States</a></li>\n			</ul>\n			<ul>\n				<li><a><i class="fab fa-facebook-f"></i></a></li>\n				<li><a><i class="fab fa-twitter"></i></a></li>\n				<li><a><i class="fab fa-linkedin-in"></i></a></li>\n			</ul>\n		</div>\n	</footer>\n	<!----footer-->\n	\n</ion-content>	'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\fourtyb\fourtyb.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]])
    ], FourtybPage);
    return FourtybPage;
}());

//# sourceMappingURL=fourtyb.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(389);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__agm_core__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_states_states__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_component__ = __webpack_require__(723);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angularfire2__ = __webpack_require__(724);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angularfire2_auth__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_auth_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ultimate_ngxerrors__ = __webpack_require__(725);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_rest__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_common_http__ = __webpack_require__(379);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__["a" /* TelehealthPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_states_states__["a" /* StatesPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__["a" /* FourtybPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_19__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__["a" /* HomePage */], name: 'Home', segment: 'home' },
                        { component: __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__["a" /* TelehealthPage */], name: 'Telehealth', segment: 'telehealth' },
                        { component: __WEBPACK_IMPORTED_MODULE_9__pages_states_states__["a" /* StatesPage */], name: 'States', segment: '1115states' },
                        { component: __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__["a" /* FourtybPage */], name: 'Fourty', segment: '340b' },
                        { component: __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */], name: 'Login', segment: 'login' },
                        { component: __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */], name: 'Signup', segment: 'signup' }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_0__agm_core__["a" /* AgmCoreModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_14_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_6__config__["b" /* firebaseConfig */].fire),
                __WEBPACK_IMPORTED_MODULE_17__ultimate_ngxerrors__["a" /* NgxErrorsModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__["a" /* TelehealthPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_states_states__["a" /* StatesPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__["a" /* FourtybPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__config__["a" /* Config */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_15_angularfire2_auth__["AngularFireAuth"],
                __WEBPACK_IMPORTED_MODULE_16__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_18__services_rest__["a" /* RestProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_app__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase_app__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = (function () {
    function AuthService(afAuth) {
        var _this = this;
        this.afAuth = afAuth;
        afAuth.authState.subscribe(function (user) {
            _this.user = user;
        });
    }
    AuthService.prototype.signInWithEmail = function (credentials) {
        console.log('Sign in with email');
        return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
    };
    AuthService.prototype.signUp = function (credentials) {
        return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
    };
    Object.defineProperty(AuthService.prototype, "authenticated", {
        get: function () {
            return this.user !== null;
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.getEmail = function () {
        return this.user && this.user.email;
    };
    AuthService.prototype.signOut = function () {
        return this.afAuth.auth.signOut();
    };
    AuthService.prototype.signInWithGoogle = function () {
        console.log('Sign in with google');
        return this.oauthSignIn(new __WEBPACK_IMPORTED_MODULE_2_firebase_app__["auth"].GoogleAuthProvider());
    };
    AuthService.prototype.oauthSignIn = function (provider) {
        if (!window.cordova) {
            return this.afAuth.auth.signInWithPopup(provider);
        }
        else {
            /*return this.afAuth.auth.signInWithRedirect(provider)
            .then(() => {
                return this.afAuth.auth.getRedirectResult().then( result => {
                    // This gives you a Google Access Token.
                    // You can use it to access the Google API.
                    let token = result.credential.accessToken;
                    // The signed-in user info.
                    let user = result.user;
                    console.log(token, user);
                }).catch(function(error) {
                    // Handle Errors here.
                    alert(error.message);
                });
            });*/
        }
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home_page__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = (function () {
    function LoginPage(navCtrl, auth, fb) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.loginForm = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].email])],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].minLength(6)])]
        });
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        var data = this.loginForm.value;
        if (!data.email) {
            return;
        }
        var credentials = {
            email: data.email,
            password: data.password
        };
        this.auth.signInWithEmail(credentials)
            .then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home_page__["a" /* HomePage */]); }, function (error) { return _this.loginError = error.message; });
    };
    LoginPage.prototype.loginWithGoogle = function () {
        var _this = this;
        this.auth.signInWithGoogle()
            .then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home_page__["a" /* HomePage */]); }, function (error) { return console.log(error.message); });
    };
    LoginPage.prototype.signupbtn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\login\login.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Log in</ion-title>\n	</ion-navbar>\n</ion-header>\n\n<ion-content>\n	\n<section class="form-section">\n	<div class="form-inner">\n		<div class="form-graphic">\n			<img src="../../assets/images/vector.png">\n		</div>\n		<div class="form-design">\n			<img src="../../assets/images/logo.png">\n		\n			<form (ngSubmit)="login()" [formGroup]="loginForm">\n				<p class="form_heading">Login</p>\n		<ion-list inset>\n\n			<ion-item [ngClass]="{ invalid: emailErrors.hasError(\'*\', [\'touched\', \'dirty\']) }">\n				<ion-input type="text" placeholder="Email" formControlName="email"></ion-input>\n			</ion-item>\n\n			<div ngxErrors="email" #emailErrors="ngxErrors">\n				<div [ngxError]="[\'email\', \'required\']" [when]="[\'touched\', \'dirty\']">It should be a valid email</div>\n			</div>\n\n			<ion-item [ngClass]="{ invalid: passwordErrors.hasError(\'*\', [\'touched\']) }">\n				<ion-input type="password" placeholder="Password" formControlName="password"></ion-input>\n			</ion-item>\n\n			<div ngxErrors="password" #passwordErrors="ngxErrors">\n				<div [ngxError]="[\'minlength\', \'required\']" [when]="[\'touched\']">It should be at least 5 characters</div>\n			</div>\n		</ion-list>\n\n		<div padding-horizontal>\n			<div class="form-error">{{loginError}}</div>\n\n			<button ion-button full type="submit" [disabled]="!loginForm.valid">Log in</button>\n			<div class="login-footer">\n				<p>\n					<a href="#">Forgot password?</a>\n					If you\'re a new user, please sign up.\n				</p>\n			</div>\n\n			<ion-list>\n\n				<button ion-button icon-left block clear (click)="signupbtn()">\n					<ion-icon name="person-add"></ion-icon>\n					Sign up\n				</button>\n			</ion-list>\n		</div>\n	</form>\n			\n		</div>\n	</div>\n</section>\n\n</ion-content>'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = 'assets/js/';
        console.log('API enable');
    }
    RestProvider.prototype.getState = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'sudstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getState1115 = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + '1115states.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getState340b = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + '340bstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getStateTelehealth = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'telehealthstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCategoryTelehealth = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'category.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_telehealth_telehealth__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_states_states__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_fourtyb_fourtyb__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_signup_signup__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_auth_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = (function () {
    function MyApp(platform, menu, statusBar, auth) {
        this.platform = platform;
        this.menu = menu;
        this.statusBar = statusBar;
        this.auth = auth;
        this.initializeApp();
        // set our app's pages
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__["a" /* HomePage */], icon: 'home' },
            { title: 'Telehealth', component: __WEBPACK_IMPORTED_MODULE_4__pages_telehealth_telehealth__["a" /* TelehealthPage */], icon: 'logo-wordpress' },
            { title: '1115 States', component: __WEBPACK_IMPORTED_MODULE_5__pages_states_states__["a" /* StatesPage */], icon: 'logo-wordpress' },
            { title: '340B', component: __WEBPACK_IMPORTED_MODULE_6__pages_fourtyb_fourtyb__["a" /* FourtybPage */], icon: 'logo-wordpress' },
            { title: 'Signup', component: __WEBPACK_IMPORTED_MODULE_8__pages_signup_signup__["a" /* SignupPage */], icon: 'logo-wordpress' }
        ];
        //this.rootPage = LoginPage;
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
        });
        this.auth.afAuth.authState
            .subscribe(function (user) {
            if (user) {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__["a" /* HomePage */];
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
            }
        }, function () {
            alert(123);
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        });
    };
    MyApp.prototype.login = function () {
        this.menu.close();
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.logout = function () {
        this.menu.close();
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__["a" /* HomePage */]);
    };
    MyApp.prototype.openPage = function (page) {
        var _this = this;
        this.menu.close();
        this.auth.afAuth.authState
            .subscribe(function (user) {
            if (user) {
                _this.nav.setRoot(page.component);
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
            }
        }, function () {
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n	\n\n	<ion-content>\n\n		<ion-list>\n\n			<ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n				<ion-icon [name]="p.icon" item-left></ion-icon>\n\n				{{p.title}}\n\n			</ion-item>\n\n			<ion-list-header *ngIf="auth.getEmail()">{{auth.getEmail()}}</ion-list-header>\n\n\n\n<ion-item (click)="logout()" *ngIf="auth.authenticated">\n\n	<ion-icon name="log-out" item-left></ion-icon>\n\n	Log out\n\n</ion-item>\n\n	\n\n<ion-item (click)="login()" *ngIf="!auth.authenticated">\n\n	<ion-icon name="log-in" item-left></ion-icon>\n\n	Log in\n\n</ion-item>\n\n		</ion-list>\n\n	</ion-content>\n\n\n\n</ion-menu>\n\n\n\n<ion-nav id="nav" [root]="rootPage" #content swipe-back-enabled="false"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_9__services_auth_service__["a" /* AuthService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__telehealth_telehealth__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = (function () {
    function HomePage(nav, restProvider, auth, navCtrl) {
        this.restProvider = restProvider;
        this.auth = auth;
        this.navCtrl = navCtrl;
        this.nav = nav;
        this.getStates();
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories', 'sud', '1');
            tableIonic().setData(tabledata);
        }, TIME_IN_MS);
    };
    HomePage.prototype.goToTelehealthPage = function (pageName) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__telehealth_telehealth__["a" /* TelehealthPage */]);
    };
    HomePage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState()
            .then(function (data) {
            _this.states = data;
            //console.log(this.states);
        });
    };
    ;
    HomePage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.onSelectChange = function (selectedValue) {
        checkJsonsud(selectedValue);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\home\home.html"*/'<ion-header> \n\n	\n\n	<ion-navbar>\n\n		<button ion-button menuToggle>\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<div class="logo">\n\n			<img src="../../assets/images/logo.jpg">\n\n		</div> \n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content class="page-home" cache-view="false" view-title="My Title!"> \n\n	<!---header--->\n\n	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">\n\n	\n\n	\n\n	\n\n	<header>\n\n		<div class="header_section">\n\n			<div class="container">\n\n				<div class="logo">\n\n					<img src="../../assets/images/logo.jpg">\n\n				</div>\n\n				\n\n				<div class="nav">\n\n					<ul>\n\n						<li><a href="/#/340b">340B</a></li>\n\n						<li><a href="/#/telehealth">Telehealth</a></li>\n\n						<li><a href="/#/1115states">1115 States</a></li>\n\n						<li><a href="/#/home" class="active">Sud</a></li>\n\n						\n\n						<li><a (click)="logout()" *ngIf="auth.authenticated">\n\n							Log out</a>\n\n						</li>\n\n							\n\n						<li><a (click)="login()" *ngIf="!auth.authenticated">\n\n							Log in</a>\n\n						</li>\n\n					</ul>\n\n				</div>\n\n				<h4>substance  use disorder</h4>\n\n			</div>\n\n		</div>\n\n	</header>\n\n	\n\n	\n\n	<!------>\n\n	\n\n	<!---section-->\n\n	<section>\n\n		\n\n		<div class="medi_coverage">\n\n			<img src="../../assets/images/energy.png">\n\n		</div>\n\n		<div class="sec_outr">\n\n			<div class="container">\n\n				<h5>Medicaid Coverage Waivers: State Profiles</h5>\n\n				<h6>11.11.18</h6>\n\n				<a class="butttonn" href="">Download Attachment</a>\n\n				<p>Section 1115 demonstrations permit states to waive certain Medicaid statutory requirements to advance state policy priorities and test innovations in their Medicaid programs, provided the Secretary of Health and Human Services determines that the demonstration “furthers the goals of the Medicaid program” and is budget neutral. States have used 1115 waiver authority to implement demonstrations ranging from Medicaid managed care programs to delivery system and payment reform initiatives. In recent years, states have leveraged 1115 waivers to modify features of Medicaid coverage for the expansion population under the Affordable Care Act (ACA) as well as traditional Medicaid populations.</p>\n\n				<p>The Trump Administration is encouraging states to pursue flexibility in administering their Medicaid programs through 1115 demonstrations that modify eligibility requirements and benefit design. In January 2018, the Centers for Medicare and Medicaid Services (CMS) released long-anticipated guidance on crafting and implementing a work and community engagement requirement as a condition of Medicaid eligibility. Following the release of the guidance, CMS approved 1115 waivers from Arkansas, Indiana, Kentucky, and New Hampshire, the first four waivers to permit such requirements in the Medicaid program, and on June 1, 2018, Arkansas became the first state to launch its work and community engagement requirement. The future of work and community engagement requirements is in question after a U.S. District Court judge issued a ruling in Stewart v. Azar in late June that invalidated HHS’s approval of Kentucky’s Medicaid waiver for failing to consider how the waiver furthered the goals of the Medicaid program. In August, Medicaid beneficiaries in Arkansas filed a lawsuit challenging HHS’s approval of that State’s Medicaid waiver to implement work and community engagement requirements.\n\n				</p>\n\n				<p>The Trump Administration has also recently begun to establish some guardrails on the changes to Medicaid that it will permit through 1115 waivers. In recent months, CMS has denied Kansas’s request to impose lifetime Medicaid enrollment limits and Massachusetts\'s request to waive Section 1927 to implement a closed prescription drug formulary, while continuing to receive the rebates required by federal law. Additionally, CMS has stated it will not approve Arkansas\'s or Massachusetts\'s request to implement partial expansion—in which it would reduce its Medicaid eligibility level to 100 percent of the federal poverty level, while continuing to receive the enhanced federal medical assistance percentage authorized under the ACA—\'at this time. The Administration\'s priorities will continue to emerge as CMS issues decisions on recently submitted state waiver proposals, including requests for work and community engagement requirements in non-expansion states, drug testing as a condition of Medicaid eligibility, and asset tests, among others.</p>\n\n				<p>This document inventories and compares the features of state Medicaid coverage waivers</p>	\n\n			</div>\n\n			\n\n			<div class="dropdownn_Sec">\n\n				<div class="container">\n\n					<h5>Select States</h5>\n\n					<div class="dropdown_Sec">\n\n						<div class="container">\n\n							<ion-item>\n\n						  <ion-label>States</ion-label>\n\n						  <ion-select name="statename" (ionChange)="onSelectChange($event)" >\n\n							<ion-option [disabled]="state.available == 0" *ngFor="let state of states" value="{{state.name}}" >{{state.name}}</ion-option>\n\n						  </ion-select>\n\n						</ion-item>\n\n						</div>\n\n						</div>\n\n					<div class="drop_Sec">\n\n						<i class="fas fa-question-circle"></i>\n\n						<a onclick="show_chart(1)"><i class="fas fa-chart-line" ></i></a>\n\n						<a onclick="show_map(1)"><i class="fas fa-globe-americas" ></i></a>\n\n					</div>\n\n				</div>\n\n			</div>\n\n		</div>\n\n		<div class="container">\n\n		<div class="map_section">\n\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map1"></div>       \n\n		</div>\n\n		</div>\n\n		<div id="state_data"></div>\n\n		\n\n		\n\n		\n\n		<div class="table_hidee">\n\n			<div class="table_hide">\n\n				<div id="example-table-theme-semantic-ui"></div>\n\n				<div class="guide">\n\n					<ul>\n\n						<li><span><img src="../../blue.png"></span><h5>= Waiver obtained </h5></li>\n\n						<li><span><img src="../../green.png"></span><h5> = Waiver requested</h5></li>\n\n						<li><span><img src="../../red.png"></span><h5>= No waiver required</h5></li>\n\n					</ul>\n\n				</div>\n\n				<div class="table-controls">\n\n					\n\n					<button id="download-xlsx">Download XLSX</button>\n\n					\n\n				</div>\n\n			</div>\n\n		</div>\n\n		\n\n	</section>\n\n	<!--section-->\n\n	\n\n	\n\n	\n\n	<!----footer-->\n\n    <footer>\n\n		<div class="foot_section">\n\n			<img src="../../assets/images/logo.jpg">\n\n			<ul class="foot-nav">\n\n				<li><a>Privacy</a></li>\n\n				<li><a href="/#/340b">340B</a></li>\n\n				<li><a href="/#/1115states" >1115 States</a></li>\n\n			</ul>\n\n			<ul>\n\n				<li><a><i class="fab fa-facebook-f"></i></a></li>\n\n				<li><a><i class="fab fa-twitter"></i></a></li>\n\n				<li><a><i class="fab fa-linkedin-in"></i></a></li>\n\n			</ul>\n\n		</div>\n\n	</footer>\n\n	<!----footer-->\n\n	\n\n</ion-content>	'/*ion-inline-end:"C:\Users\DESTOP\Desktop\git-qa\state-survey\src\pages\home\home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */],
            __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.page.js.map

/***/ })

},[384]);
//# sourceMappingURL=main.js.map