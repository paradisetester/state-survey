import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Config } from '../config';
import { HomePage } from '../pages/home/home.page';
import { TelehealthPage } from '../pages/telehealth/telehealth';
import { StatesPage } from '../pages/states/states';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { FourtybPage } from '../pages/fourtyb/fourtyb';

import { MyApp } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';
import { AuthService } from '../services/auth.service';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { RestProvider } from '../services/rest';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
	declarations: [
		MyApp,
		HomePage,
		LoginPage,
		TelehealthPage,
		SignupPage,
		StatesPage,
		FourtybPage
	],
	imports: [
		BrowserModule,
		HttpModule,
		HttpClientModule,
		IonicModule.forRoot(MyApp, {}, {
		 links: [
		  { component: HomePage, name: 'Home', segment: 'home' },
		  { component: TelehealthPage, name: 'Telehealth', segment: 'telehealth' },
		  { component: StatesPage, name: 'States', segment: '1115states' },
		  { component: FourtybPage, name: 'Fourty', segment: '340b' },
		  { component: LoginPage, name: 'Login', segment: 'login' },
		  { component: SignupPage, name: 'Signup', segment: 'signup' }
		]
	  }),
		AgmCoreModule.forRoot(),
		AngularFireModule.initializeApp(firebaseConfig.fire),
		
		NgxErrorsModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		LoginPage,
		TelehealthPage,
		HomePage,
		StatesPage,
		SignupPage,
		FourtybPage
	],
	providers: [
		Config,
		StatusBar,
		AngularFireAuth,
		AuthService,
		RestProvider
	]
})
export class AppModule {
}
