import { Component, ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';

import { MenuController, Nav, Platform } from 'ionic-angular';
import { HomePage } from '../pages/home/home.page';
import { TelehealthPage } from '../pages/telehealth/telehealth';
import { StatesPage } from '../pages/states/states';
import { FourtybPage } from '../pages/fourtyb/fourtyb';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { AuthService } from '../services/auth.service';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	pages;
	rootPage;

	@ViewChild(Nav) nav: Nav;

	constructor(
		private platform: Platform,
		private menu: MenuController,
		private statusBar: StatusBar,
		private auth: AuthService
	) {
		this.initializeApp();

		// set our app's pages
		this.pages = [
			{ title: 'Home', component: HomePage, icon: 'home' },
			{ title: 'Telehealth', component: TelehealthPage, icon: 'logo-wordpress' },
			{ title: '1115 States', component: StatesPage, icon: 'logo-wordpress' },
			{ title: '340B', component: FourtybPage, icon: 'logo-wordpress' },
			{ title: 'Signup', component: SignupPage, icon: 'logo-wordpress' }
		];

		//this.rootPage = LoginPage;
	}

	initializeApp() {
		  this.platform.ready().then(() => {
			this.statusBar.styleDefault();
		  });

		  this.auth.afAuth.authState
			.subscribe(
			  user => {
				if (user) {
				  this.rootPage = HomePage;
				} else {
				  this.rootPage = LoginPage;
				}
			  },
			  () => {
			    alert(123);
				this.rootPage = LoginPage;
			  }
			);
		}


	login() {
		this.menu.close();
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
	
	logout() {
		this.menu.close();
		this.auth.signOut();
		this.nav.setRoot(HomePage);
	}

	openPage(page) {
		this.menu.close();
		this.auth.afAuth.authState
			.subscribe(
			  user => {
				if (user) {
					this.nav.setRoot(page.component);
				} else {
				  this.rootPage = LoginPage;
				}
			  },
			  () => {
				this.rootPage = LoginPage;
			  }
			);
			
	}
}
