import { Component } from '@angular/core';
import { Nav,NavController } from 'ionic-angular';
import { data } from './home-data';
import { LoginPage } from '../login/login';
import { TelehealthPage } from '../telehealth/telehealth';
import { RestProvider } from '../../services/rest';
import { AuthService } from '../../services/auth.service';
import * as $ from "jquery";
declare var checkJsonsud: any;
declare var tabledatasud: any;
declare var tableIonic: any;
declare var USAmapLoad: any;
declare var tabledata: any;

@Component({
	templateUrl: 'home.html',
	providers: [RestProvider]
})
export class HomePage {	
	states : any;	
	private nav: Nav;
	
	constructor(		
	nav: Nav,
	public restProvider: RestProvider,
	private auth: AuthService,
	public navCtrl: NavController,
	) {
		this.nav = nav;
		this.getStates();		
	}
	   
	ionViewDidLoad(){	
	  let TIME_IN_MS = 500;
	  let hideFooterTimeout = setTimeout( () => {
		USAmapLoad('usaTerritories','sud','1');		
		tableIonic().setData(tabledata);		
	  }, TIME_IN_MS);  
	 }
	
	goToTelehealthPage(pageName){
		this.navCtrl.push(TelehealthPage);
	}
	 
	getStates() {
		this.restProvider.getState()
		.then(data => {
			this.states = data;
			//console.log(this.states);
		});
	};
	
	
	logout() {
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
	onSelectChange(selectedValue: any) {    
		checkJsonsud(selectedValue); 
		
	}   
	
 
}
