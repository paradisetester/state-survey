import { Component } from '@angular/core';
import { Nav, IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { LoginPage } from '../login/login';
import { RestProvider } from '../../services/rest';
import { AuthService } from '../../services/auth.service'; 
/**
 * Generated class for the TelehealthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */  
import * as $ from "jquery";
declare var checkJson: any;
declare var USAmapLoad: any;
declare var categoryChange: any;
declare var stateChange: any;
         
@IonicPage()
@Component({
  selector: 'page-telehealth',
  templateUrl: 'telehealth.html',
})
export class TelehealthPage {
  states : any;
  category : any;
  constructor(public nav: Nav, public navCtrl: NavController, public restProvider: RestProvider, public http: HttpModule,private auth: AuthService) {
		this.getStates();	
		this.getCategory();	
  } 
     
	ionViewDidLoad(){	     
	  let TIME_IN_MS = 500;
	  let hideFooterTimeout = setTimeout( () => {			
			USAmapLoad('usaTerritories1','telehealth','2');	
	  }, TIME_IN_MS);  
	 } 
	getStates() {
	this.restProvider.getStateTelehealth()
	.then(data => {
	  this.states = data;      
	});
	};
	getCategory() {
	this.restProvider.getCategoryTelehealth()
	.then(data => {
	  this.category = data;      
	});
	};
   
	onSelectChange(selectedValue: any) { 
		stateChange(selectedValue); 
	}
	onCategoryChange(selectedValue: any) {
		categoryChange(selectedValue); 
	}
	
	logout() {
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
	
}
 