import { Component } from '@angular/core';
import { IonicPage,Nav, NavController, NavParams } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { LoginPage } from '../login/login';
import { RestProvider } from '../../services/rest';
import { AuthService } from '../../services/auth.service';
/**
	* Generated class for the StatesPage page.
	*
	* See https://ionicframework.com/docs/components/#navigation for more info on
	* Ionic pages and navigation.
*/
import * as $ from "jquery";
declare var checkJson1115: any;
declare var USAmapLoad: any;

declare var stateChange: any;


@IonicPage()
@Component({
	selector: 'page-states',
	templateUrl: 'states.html',
})
export class StatesPage {
	states : any;
    constructor(public nav: Nav, public navCtrl: NavController, public restProvider: RestProvider, public http: HttpModule,private auth: AuthService) {
		this.getStates();	
	}  
  	ionViewDidLoad(){
		console.log('ad');
		let TIME_IN_MS = 500;
		let hideFooterTimeout = setTimeout( () => {
			USAmapLoad('usaTerritories3','1115','3');
		}, TIME_IN_MS);  
	}
	getStates() {
		this.restProvider.getState1115()
		.then(data => {		
			this.states = data; 		  
		});
	};
	
	onSelectChange(selectedValue: any) {    
		checkJson1115(selectedValue); 
		
	  }
	
	
	logout() {
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
}
