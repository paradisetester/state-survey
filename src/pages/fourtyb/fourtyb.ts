import { Component } from '@angular/core';
import { Nav, IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { LoginPage } from '../login/login';
import { RestProvider } from '../../services/rest';
import { AuthService } from '../../services/auth.service';
    
/**
 * Generated class for the FourtybPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 import * as $ from "jquery";
declare var checkJson340b: any;
declare var USAmapLoad: any;

@IonicPage()
@Component({
  selector: 'page-fourtyb',
  templateUrl: 'fourtyb.html',
})
export class FourtybPage {
	states : any;
  constructor(public nav: Nav, public navCtrl: NavController, public restProvider: RestProvider, public http: HttpModule,private auth: AuthService) {
		this.getStates();		
  }
  
  getStates() {
	this.restProvider.getState340b()
	.then(data => {
	  this.states = data;      
	});
	};
	onSelectChange(selectedValue: any) { 
		checkJson340b(selectedValue); 
	}
	  
  ionViewDidLoad() {		
		USAmapLoad('usaTerritories4','340b','4');	
  }
logout() {
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
}
